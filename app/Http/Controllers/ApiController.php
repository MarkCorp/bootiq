<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\BranchModel;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\BusinessHourModel;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

class ApiController extends Controller
{

    /**
     * index
     *
     * @param  mixed $endpoint_id
     * @return void
     */
    public function index($endpoint_id){

        $branch = Branch::where("long_id", $endpoint_id)->first();
        $branch = collect($branch->data_all());
        $get_url = "/" . $endpoint_id;

        $branches = $this->paginate($branch, 25, null, ["path" => $get_url]);
        $all_branches = Branch::all();
        return view("welcome")->with("branches", $branches)->with("all_branches", $all_branches);
    }

    /**
     * show
     *
     * @param  mixed $endpoint_id
     * @param  mixed $id
     * @return void
     */
    public function show($endpoint_id, $id){
        // $geolocation = "49.2715599,21.8932915";
        // dd(Http::get('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false&key=AIzaSyAVOED3j3Dl7Zz9_yGZaH0apy0BkEAjYLg')->body());

        $branch = Branch::where("long_id", $endpoint_id)->first();
        foreach($branch->data_all() as $item){
            if($item->id == $id){
                $branch_details = new BranchModel($item, $id);
            }
        }

        return view("details")->with("branch_details", $branch_details);
    }

    /**
     * save
     *
     * @param  mixed $request
     * @return void
     */
    public function save(Request $request){

        // Validate data
        $validator = Validator::make($request->all(), [
            'end_point' => 'required|unique:branches|max:255',
        ]);

        if ($validator->fails()) {
            Session::put("endpoint_saved", ["alert-danger", "Api Error!"]);
            return back();
        }

        // Save new Api EndPoint
        $url_api = new Branch;
        $url_api->end_point = $request->end_point;
        $url_api->long_id = Str::random(15);
        $url_api->save();

        Session::put("endpoint_saved", ["alert-success", "Api EndPoint saved!"]);
        return back();
    }


    /**
     * paginate
     *
     * @param  mixed $items
     * @param  mixed $perPage
     * @param  mixed $page
     * @param  mixed $options
     * @return void
     */
    private function paginate($items, $perPage = 25, $page = null, $options = []){
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options, );
    }
}
