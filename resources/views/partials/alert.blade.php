@if (Session::has("endpoint_saved"))
@php
    $alert_type = Session::get("endpoint_saved")[0];
    $message = Session::get("endpoint_saved")[1];
@endphp

    <div class="alert {{ $alert_type }}" role="alert">

        <p class="d-inline">{{ $message }}</p>

        @php
                Session::forget("endpoint_saved");
        @endphp

    </div>
@endif
