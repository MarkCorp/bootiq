<div class="row mt-5 table-holder">
    <div class="col-md-12">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Num. Register</th>
                    <th scope="col">Active</th>
                    <th scope="col">Show</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($branches as $key => $item)
                        <tr>
                            <th scope="row">{{ $key }}</th>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->register }}</td>
                            <td>{{ $item->active }}</td>
                            <td><a href="{{ url("/details") . "/" . Request::segment(1) . "/" . $item->id }}"><i class="fas fa-eye"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
              </table>

              {{ $branches->links() }}
    </div>
</div>
