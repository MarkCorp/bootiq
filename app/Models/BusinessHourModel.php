<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessHourModel extends Model
{
    use HasFactory;
    /** @var string */ private $dayOfWeek;
    /** @var string */ private $businessHour;

    /**
     * calc_day
     *
     * @param  mixed $num_day
     * @return void
     */
    private function calc_day($num_day){
        switch ($num_day) {
            case 1:
                $this->dayOfWeek = "Monday";
            break;
            case 2:
                $this->dayOfWeek = "Tuesday";
            break;
            case 3:
                $this->dayOfWeek = "Wednesday";
            break;
            case 4:
                $this->dayOfWeek = "Thursday";
            break;
            case 5:
                $this->dayOfWeek = "Friday";
            break;
            case 6:
                $this->dayOfWeek = "Saturday";
            break;
            case 7:
                $this->dayOfWeek = "Sunday";
            break;
            default:
                $this->dayOfWeek = "Error";
            break;
        }
    }

     /** @var string Calc Hours */
    /**
     * calc_business_hours
     *
     * @param  mixed $open
     * @param  mixed $close
     * @return void
     */
    private function calc_business_hours($open, $close){
        $this->businessHour = $open . " - " . $close;
    }

     /** @var string GET DAY OF WEEK */
    public function get_dayOfWeek($num_day){
        $this->calc_day($num_day);

        return $this->dayOfWeek;
    }

    /** @var string GET HOURS */
    public function get_businessHours($open, $close){
        $this->calc_business_hours($open, $close);

        return $this->businessHour;
    }
}
