@extends('layouts.app')

@section('content')
<a href="{{ url("/") . "/" . Request::segment(2) }}">Go home</a>

<div class="d-flex justify-content-center align-items-center w-100 mt-5">
    <div class="row table-holder w-100 h-100 data-details-holder">
        <div class="div-col-md-12">
            <h1>{{ $branch_details->get_internalName() }}</h1>
            <div class="row height-50 mt-5">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <h2><i class="far fa-clock"></i> Opening hours</h2>
                            <div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Day</th>
                                        <th scope="col">Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($branch_details->get_businessHours() as $item)
                                            <tr>
                                                <td>{{ $item[0] }}</td>
                                                <td>{{ $item[1] }}</td >
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12">
                            <h2><i class="fas fa-map-marker-alt"></i> GPS</h2>
                            <div>
                                {{ $branch_details->get_locations()[0] . "N, " . $branch_details->get_locations()[1] . "E " }}

                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <h2><i class="fas fa-globe-europe"></i> Web original</h2>
                            <div>
                                {{ $branch_details->get_web() }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div id="mapid"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var mymap = L.map('mapid').setView(["{{ $branch_details->get_locations()[0] }}", "{{ $branch_details->get_locations()[1] }}"], 40);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibXJlc3NoIiwiYSI6ImNrczI3eHd3dDFqdWEycW85NHlncGs2bncifQ._AcG7Fro-KxyTbqMJTk0cw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibXJlc3NoIiwiYSI6ImNrczI3eHd3dDFqdWEycW85NHlncGs2bncifQ._AcG7Fro-KxyTbqMJTk0cw'
}).addTo(mymap);

var marker = L.marker(["{{ $branch_details->get_locations()[0] }}", "{{ $branch_details->get_locations()[1] }}"]).addTo(mymap);



// var circle = L.circle([51.508, -0.11], {
//     color: 'red',
//     fillColor: '#f03',
//     fillOpacity: 0.5,
//     radius: 500
// }).addTo(mymap);

// var polygon = L.polygon([
//     [51.509, -0.08],
//     [51.503, -0.06],
//     [51.51, -0.047]
// ]).addTo(mymap);

</script>
@endsection
