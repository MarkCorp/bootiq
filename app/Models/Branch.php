<?php

namespace App\Models;

use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Branch extends Model
{
    use HasFactory;

    /**
     * data_all
     *
     * @return void
     */
    public function data_all(){
        $data = Http::get($this->end_point);
        $data = $data->body();

        return json_decode($data);
    }

    /**
     * iternatlId
     *
     * @return void
     */
    public function iternatlId(){
        $data = Http::get($this->end_point);
        $data = $data->body();

        return json_decode($data);
    }

    /**
     * end_point
     *
     * @return void
     */
    public function end_point(){
        return $this->end_point;
    }
}
