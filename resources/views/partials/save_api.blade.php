<div class="row mt-5 mb-5">
    <div class="col-md-12">
        <h3>All Endpoints</h3>
        @foreach ($all_branches as $item)

        <a href="{{ url("/") . "/" . $item->long_id }}" class="btn btn-primary">
           {{ $item->end_point  }}
        </a>
        @endforeach

    </div>
</div>
<div class="row table-holder">
        <div class="col-md-12">
            <form action="{{ url("save/api") }}" method="POST">

                @csrf

                <div class="mb-3">
                  <label for="saveApi" class="form-label">Save EndPoint (URL)</label>
                  <input type="text" class="form-control" id="saveApi" name="end_point">
                  <div id="saveApi" class="form-text">You can save your Api's</div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
