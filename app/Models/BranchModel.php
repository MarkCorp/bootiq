<?php

namespace App\Models;

use App\Models\BusinessHourModel;

class BranchModel
{
    /** @var string */ private $internalId;
    /** @var string */ private $internalName;
    /** @var Coordinates */ private $location;
    /** @var array<BusinessHourModel> */ private $businessHours;
    /** @var string */ private $address;
    /** @var string */ private $web;
    /** @var string */ private $announcement;

    public function __construct($data, $id){

        $this->internalId = isset($data->id) ? $data->id : null;
        $this->internalName = isset($data->name) ? $data->name : null;
        $this->location = (isset($data->lat) && isset($data->lng)) ? [$data->lat, $data->lng] : null;

        //? NO DATA //I tried geocode via Google but https required || In case I can add https
        // $this->address = $data->adress;
        // $geolocation = "49.2715599,21.8932915";
        // dd(Http::get('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false&key=AIzaSyAVOED3j3Dl7Zz9_yGZaH0apy0BkEAjYLg')->body());

        $this->web = isset($data->odkaz) ? $data->odkaz : null;
        $this->announcement = isset($data->announcements) ? $data->announcements : null;


        if(isset($data->openingHours)){
            $date_array = $this->calc_BusinessHourModel($data->openingHours);

            $this->businessHours = $date_array;
        }
    }

    /**
     * calc_BusinessHourModel
     *
     * @param  mixed $openingHours
     * @return void
     */
    private function calc_BusinessHourModel($openingHours){
        $BusinessHourModel_calc = new BusinessHourModel;
        $date_array = [];


        foreach ($openingHours as $value) {
           $skip = false;
           $first_time = false;
           $day = $BusinessHourModel_calc->get_dayOfWeek($value->day);
           $hours = $BusinessHourModel_calc->get_businessHours($value->open, $value->close);

           if($date_array == []){
                $date_array[] = [$day, $hours];
                $first_time = true;
                $skip = true;
           }

            foreach ($date_array as $data_item) {
                if(in_array($day, $data_item)){
                    foreach ($date_array as $data_key => $data_value) {
                        if($data_value[0] == $day && $first_time == false){
                            $date_array[$data_key] = [$day, $data_value[1] . " | " .  $hours];
                            $skip = true;
                        }
                    }
                }
            }

            if($skip == false){
                $date_array[] = [$day, $hours];
            }

        }

        return $date_array;
    }

    /**
     * get_id
     *
     * @return void
     */
    public function get_id(){
        return $this->internalId;
    }

    /**
     * get_internalName
     *
     * @return void
     */
    public function get_internalName(){
        return $this->internalName;
    }

    /**
     * get_locations
     *
     * @return void
     */
    public function get_locations(){
        return $this->location;
    }

    /**
     * get_web
     *
     * @return void
     */
    public function get_web(){
        return $this->web;
    }

    /**
     * get_announcement
     *
     * @return void
     */
    public function get_announcement(){
        return $this->announcement;
    }

    /**
     * get_businessHours
     *
     * @return void
     */
    public function get_businessHours(){
        return $this->businessHours;
    }
}
