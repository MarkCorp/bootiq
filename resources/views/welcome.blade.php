@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Show & Save API data</h1>
    @include("partials.save_api")
    @include('partials.show_api_data')
    <footer class="pt-5 pb-5">
        Fast created by <a href="https://www.smetankawebdev.sk">Marek Smetanka</a>
    </footer>
</div>
@endsection

