<?php

use App\Models\Branch;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "HomePage | Doesn't created yet!";
});
Route::get("/{endpoint_id}", "App\Http\Controllers\ApiController@index");
Route::post("save/api", "App\Http\Controllers\ApiController@save");
Route::get("/details/{endpoint_id}/{id}", "App\Http\Controllers\ApiController@show");

